import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import {setLang, switchBrand, storage} from './utils';
import store from './store';
import Root from './root';
import './global.less';

setLang();
ReactDOM.render(
  <Provider store={store}>
    <Root />
  </Provider>,
  document.getElementById('root'),
  () => {
    switchBrand(storage.get('brandBG'));
  },
);
