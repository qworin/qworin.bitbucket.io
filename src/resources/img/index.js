import objective from './objective.svg';
import contact from './mail.svg';
import xp from './briefcase.svg';
import edu from './certificate.svg';
import skill from './imac.svg';
import ru from './ru.svg';
import uk from './uk.svg';
import us from './us.svg';
import avatar from './ava.jpeg';

// as inline components:
const icons = {
  objective,
  contact,
  xp,
  edu,
  skill,
};
const flags = {
  ru,
  uk,
  us,
  en: us, // NB US as en
};

// as regular source:
const pics = {
  avatar,
};

export {flags, icons, pics};
