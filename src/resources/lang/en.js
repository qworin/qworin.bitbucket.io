/* eslint-disable no-irregular-whitespace */
import {dfn, skills, formerSkills} from './skills';

export default {
  translation: {
    documentTitle: 'CV | Rodion Strizhakov',
    header: {
      title: 'Rodion Strizhakov',
      subtitle: 'Frontend Developer (React Native), 30 years old',
      switchTitle: 'Click to switch language',
      avatar: 'Rodion Strizhakov photo',
    },
    objective: {
      title: 'Career objective',
      body: 'I’m an experienced and knowledgeable frontend developer with extensive and various practice in web and apps development with React Native. I’m looking to obtain a position that allows to applicate my skills, as well to improve them and to learn new technologies related to the app development',
    },
    contact: {
      title: 'Contact',
      email: {name: 'E-mail', value: 'qworin@pm.me'},
      phone: {name: 'Phone', value: '+79138998867'},
      address: {name: 'Address', value: 'Novosibirsk, Akademgorodok'},
    },
    skills: {title: 'Skills', items: skills},
    experience: {
      title: 'Experience (3.7 years as developer)',
      items: [
        {
          name: 'Frontend Developer',
          location: 'Digital Ecosystems, Feb 2019 – Aug 2020',
          list: [
            `I developed, maintained, and monetized several iOS and UWP apps with ${skills[2]}. On the other side there was frontend development of several web sites with ${skills[1]}, ${skills[4]}, and a bit of ${skills[3]}.`,
            `As employee of Digital Ecosystems I focused on my frontend skills and experienced well in modern frontend technologies expecially in ${skills[1]}.`,
          ],
        },
        {
          name: 'Web Developer',
          location: 'e2e4gu.ru, Jan 2017 – Jan 2019',
          list: [
            'I experienced diverse commercial projects on development of web and hybrid apps, sometimes along with backend, and CMS management.',
            `Some key technologies: ${skills[0]}, ${formerSkills.join(', ')}.`,
          ],
        },
        {
          name: 'Scientist',
          location: 'SB RAS instututes, Mar 2010 – Jan 2017',
          list: [
            `I did fundamental scientific investigations in field of physical and biological chemistry. I got the first programming skills analyzing spectral and kinetics data with ${dfn('Matlab')} software.`,
          ],
        },
      ],
    },
    education: {
      title: 'Education',
      items: [
        {name: 'Ph. D. (physical chemistry)', location: 'Novosibirsk International Tomography center', date: '2012–2017'},
        {name: 'Diploma in chemistry', location: 'Novosibirsk State University', date: '2007–2012'},
      ],
    },
  },
};
