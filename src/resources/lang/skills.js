const wrap = (content, id = -1) => `<dfn${id >= 0 ? ` data-skill="${id}"` : ''}>${content}</dfn>`;

const skills = [
  'Javascript',
  'React',
  'React Native',
  'Vue',
  'Less',
].map((item, index) => wrap(item, index));

const formerSkills = [
  'PHP',
  'MySQL',
  'SASS',
  'Typescript',
  'Ionic 2–3',
  'Angular 5',
  'CakePHP',
  'Bootstrap',
  'Wordpress',
  'Prestashop',
  'Symfony',
].map((item) => wrap(item));

export {wrap as dfn, skills, formerSkills};
