import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import {icons} from '../../../resources/img';
import './index.less';

const ArticleTitle = ({icon, label}) => {
  const Icon = icon && icons[icon];
  return (
    <h3 className="article-title">
      {Icon && (
        <div className={cn('icon', icon)}>
          <Icon />
        </div>
      )}
      <span>{label}</span>
    </h3>
  );
};

ArticleTitle.propTypes = {
  icon: PropTypes.oneOf(Object.keys(icons)),
  label: PropTypes.string,
};

ArticleTitle.defaultProps = {
  icon: null,
  label: '',
};

export default ArticleTitle;
