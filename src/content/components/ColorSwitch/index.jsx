import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {switchBrand} from '../../../utils';

import './index.less';

const BrandColorSwitcher = ({brand}) => (
  <input
    className="color-switch"
    type="color"
    value={brand.bg}
    onChange={({target}) => switchBrand(target.value)}
  />
);

BrandColorSwitcher.propTypes = {
  brand: PropTypes.object.isRequired,
};

const stateToProps = ({theme: {brand}}) => ({brand});

export default connect(stateToProps)(BrandColorSwitcher);
