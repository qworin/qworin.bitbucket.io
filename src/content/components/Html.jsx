import React from 'react';
import PropTypes from 'prop-types';

const Html = ({content: __html, className}) => (
  // eslint-disable-next-line react/no-danger
  <div dangerouslySetInnerHTML={{__html}} className={className} />
);

Html.propTypes = {
  content: PropTypes.string.isRequired,
  className: PropTypes.string,
};

Html.defaultProps = {
  className: '',
};

export default Html;
