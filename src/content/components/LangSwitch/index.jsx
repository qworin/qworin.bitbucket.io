import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import cn from 'classnames';

import {flags} from '../../../resources/img';
import {setLang, getT} from '../../../utils';
import './index.less';

const En = flags.en;
const Ru = flags.ru;
const EN = 'en';
const RU = 'ru';
const toggle = (curr) => (curr !== EN ? EN : RU);
const t = getT('header');

const LangSwitcher = ({lang}) => {
  const [flag, setFlag] = useState(lang);
  useEffect(() => {
    if (lang !== flag) setLang(flag);
  }, [flag]);
  return (
    <button
      type="button"
      className={cn('lang-switch', flag !== EN && 'active')}
      onClick={() => setFlag(toggle(flag))}
      title={t('switchTitle')}
    >
      <div className="card">
        <div className="front"><En /></div>
        <div className="back"><Ru /></div>
      </div>
    </button>
  );
};

LangSwitcher.propTypes = {
  lang: PropTypes.string.isRequired,
};

const stateToProps = ({i18n: {lang}}) => ({lang});

export default connect(stateToProps)(LangSwitcher);
