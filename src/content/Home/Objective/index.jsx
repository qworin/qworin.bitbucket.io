import React from 'react';
import {getT} from '../../../utils/lang';

import Title from '../../components/ArticleTitle';
import './index.less';

const t = getT('objective');

const Objective = () => (
  <div className="objective">
    <Title label={t('title')} icon="objective" />
    <div className="body">{t('body')}</div>
  </div>
);

export default Objective;
