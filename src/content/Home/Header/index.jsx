import React from 'react';

import {getT} from '../../../utils';
import LangSwitch from '../../components/LangSwitch';
import ColorSwitch from '../../components/ColorSwitch';
import {pics} from '../../../resources/img';
import './index.less';

const t = getT('header');

const Header = () => (
  <header className="header">
    <img className="avatar" alt={t('avatar')} src={pics.avatar} />
    <div className="content">
      <h2 className="title">{t('title')}</h2>
      <h3 className="subtitle">{t('subtitle')}</h3>
    </div>
    <div className="actions">
      <ColorSwitch />
      <LangSwitch />
    </div>
  </header>
);

export default Header;
