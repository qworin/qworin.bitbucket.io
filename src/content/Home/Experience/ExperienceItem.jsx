import React from 'react';
import PropTypes from 'prop-types';

import {genId} from '../../../utils';
import Html from '../../components/Html';

const ExperienceItem = ({name, location, list}) => (
  <div className="experience">
    <h4 className="title">{name}</h4>
    <div className="subtitle">{location}</div>
    {list.map((item) => <Html key={genId()} content={item} className="item" />)}
  </div>
);

ExperienceItem.propTypes = {
  name: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default ExperienceItem;
