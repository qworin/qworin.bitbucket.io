import React from 'react';

import {getT, genId} from '../../../utils';
import Title from '../../components/ArticleTitle';
import Item from './ExperienceItem';
import './index.less';

const t = getT('experience');

const Experience = () => {
  const items = t('items', {returnObjects: true}) || [];
  return (
    <div className="experience-wrap">
      <Title label={t('title')} icon="xp" />
      {items.map((item) => <Item key={genId()} {...item} />)}
    </div>
  );
};

export default Experience;
