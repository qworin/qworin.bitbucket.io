import React from 'react';

import {getT, genId} from '../../../utils';
import Html from '../../components/Html';
import Title from '../../components/ArticleTitle';
import './index.less';

const t = getT('skills');

const Skills = () => {
  const items = t('items', {returnObjects: true}) || [];
  return (
    <div className="skills-wrap">
      <Title label={t('title')} icon="skill" />
      <div className="skills">
        {items.map((item) => (
          <Html key={genId()} content={item} className="item" />
        ))}
      </div>
    </div>
  );
};

export default Skills;
