import React from 'react';
import PropTypes from 'prop-types';

const EducationItem = ({name, location, date: dateRange}) => (
  <div className="education">
    <h4 className="title">{name}</h4>
    <div className="subtitle">{location}</div>
    <div className="subtitle">{dateRange}</div>
  </div>
);

EducationItem.propTypes = {
  name: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
};

export default EducationItem;
