import React from 'react';

import {getT, genId} from '../../../utils';
import Title from '../../components/ArticleTitle';
import Item from './EducationItem';
import './index.less';

const t = getT('education');

const Education = () => {
  const items = t('items', {returnObjects: true}) || [];
  return (
    <div className="education-wrap">
      <Title label={t('title')} icon="edu" />
      {items.map((item) => <Item key={genId()} {...item} />)}
    </div>
  );
};

export default Education;
