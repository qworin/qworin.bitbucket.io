import React from 'react';
import PropTypes from 'prop-types';

import {formatPhone, hasHtmlTag} from '../../../utils';
import Html from '../../components/Html';

const ContactItem = ({type, name, value: raw}) => {
  let value = raw;
  if (/email/i.test(type)) {
    value = <a href={`mailto:${raw}`} className="link">{raw}</a>;
  } else if (/phone/i.test(type)) {
    value = formatPhone(raw);
  }
  if (hasHtmlTag(raw)) {
    value = <Html content={value} />;
  }
  return (
    <div className="item">
      <div className="name">{name}</div>
      <div className="value">{value}</div>
    </div>
  );
};

ContactItem.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

export default ContactItem;
