import React from 'react';

import {getT} from '../../../utils';
import Title from '../../components/ArticleTitle';
import Item from './ContactItem';
import './index.less';

const t = getT('contact');
const keys = ['email', 'phone', 'address'];

const Contact = () => (
  <div className="contact">
    <Title label={t('title')} icon="contact" />
    {keys.map((key) => {
      const data = t(key, {returnObjects: true});
      if (data !== Object(data)) return null;
      data.type = key;
      return <Item key={key} {...data} />;
    })}
  </div>
);

export default Contact;
