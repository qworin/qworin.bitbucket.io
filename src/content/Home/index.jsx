import React, {useEffect} from 'react';
import {connect} from 'react-redux';

import Header from './Header';
import Objective from './Objective';
import Skills from './Skills';
import Experience from './Experience';
import Education from './Education';
import Contact from './Contact';

const Home = () => {
  useEffect(() => {}, []); // do some start stuff
  return (
    <main className="main-wrap">
      <Header />
      <main className="main">
        <Objective />
        <Experience />
      </main>
      <aside className="sidebar">
        <Contact />
        <Skills />
        <Education />
      </aside>
    </main>
  );
};

const stateToProps = ({i18n: lang}) => ({lang});

export default connect(stateToProps)(Home);
