export default (name, actions) => {
  if (!name || typeof name !== 'string' || !actions || !Array.isArray(actions)) {
    console.warn(`@createConstants error; module ${name} actions ${actions}`);
    return {};
  }
  return actions.reduce((res, action) => (
    {...res, [action]: `${name}_${action}`}
  ), {});
};
