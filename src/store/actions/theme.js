import THEME from '../constants/theme';
import {initState} from '../reducers/theme';

export const setBrand = (payload = initState) => (dispatch) => {
  dispatch({type: THEME.SET_BRAND, payload});
};
