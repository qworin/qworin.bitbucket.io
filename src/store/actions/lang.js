import LANG from '../constants/lang';

export const switchLang = (payload = 'en') => (dispatch) => {
  dispatch({type: LANG.SWITCH, payload});
};
