import {combineReducers} from 'redux';

import i18n from './lang';
import theme from './theme';

export default combineReducers({i18n, theme});
