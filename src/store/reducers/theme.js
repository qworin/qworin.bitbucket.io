import THEME from '../constants/theme';

const initState = {
  brand: {
    bg: '#4682B4',
    fg: '#FFFFFF',
  },
};

const themeReducer = (state = initState, {type, payload}) => ({
  [THEME.SET_BRAND]: {...state, brand: payload},
}[type] || state);

export {initState};
export default themeReducer;
