import LANG from '../constants/lang';

const initState = {
  lang: 'en',
};

const langReducer = (state = initState, {type, payload}) => ({
  [LANG.SWITCH]: {...state, lang: payload},
}[type] || state);

export default langReducer;
