import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';

import rootReducer from './reducers';

const middleware = process.env.NODE_ENV !== 'production' ?
  applyMiddleware(
    thunk,
    createLogger({collapsed: true}),
  ) :
  applyMiddleware(
    thunk,
  );

const initStore = (initialState) => (createStore(
  rootReducer,
  initialState,
  middleware,
));

export default initStore();
