const warn = () => {
  console.warn('There is no localStorage available');
  return null;
};

const storage = localStorage ? {
  get: (key) => localStorage.getItem(key),
  set: (key, value) => localStorage.setItem(key, value),
  remove: (key) => localStorage.removeItem(key),
} : {
  get: warn,
  set: warn,
  remove: warn,
};

export default storage;
