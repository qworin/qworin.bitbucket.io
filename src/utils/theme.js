/*
 * NB Many magic numbers ahead
 * Relative luminance evaluation & gamma correction implemented
 * in sRGB colorspace according to:
 * https://developer.mozilla.org/en-US/docs/Web/Accessibility/Understanding_Colors_and_Luminance#Measuring_Relative_Luminance
 */
import store from '../store';
import {setBrand} from '../store/actions/theme';
import storage from './storage';

const {dispatch} = store;

const validateColor = (raw) => /^#[a-f0-9]{6}$/i.test(raw);

const correctGamma = (rgbComponent) => {
  const rsRgb = parseInt(rgbComponent, 16) / 255;
  return rsRgb < 0.03928 ? rsRgb / 12.92 : ((rsRgb + 0.055) / 1.055) ** 2.4;
};

const luminanceCoeff = [0.2126, 0.7152, 0.0722];
const calcLuminance = (rgb) => rgb.match(/[a-f0-9]{2}/gi)
  .reduce((res, component, index) => (
    res + correctGamma(component) * luminanceCoeff[index]
  ), 0);

const FG_COLOR = ['#FFFFFF', '#000000'];
const chooseFG = (bg) => FG_COLOR[+(calcLuminance(bg) > 0.5)];

const switchBrand = (bgValue) => {
  const root = document.getElementById('root');
  if (!validateColor(bgValue)) return;
  const bg = bgValue;
  const fg = chooseFG(bg);
  root.style.setProperty('--brand-bg', bg);
  root.style.setProperty('--brand-fg', fg);
  setBrand({bg, fg})(dispatch);
  storage.set('brandBG', bg);
};

export {switchBrand};
