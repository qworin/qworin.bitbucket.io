import i18next from 'i18next';

import store from '../store';
import storage from './storage';
import LANG from '../store/constants/lang';
import resources from '../resources/lang';

const STORAGE_KEY = 'currentLang';

const initLang = (initLang) => {
  const browserLang = navigator.language.slice(0, 2);
  const lng = storage.get(STORAGE_KEY) || (resources[browserLang] ? browserLang : initLang);
  i18next.init({
    lng,
    fallbackLng: 'en',
    debug: true,
    resources,
  });
  return lng;
};

const setLang = (rawLang) => {
  const {i18n: {lang}} = store.getState();
  let tmp = lang;
  if (!rawLang) {
    tmp = initLang(tmp);
  } else {
    tmp = resources[rawLang] ? rawLang : lang;
    i18next.changeLanguage(tmp);
  }
  document.title = i18next.t('documentTitle');
  store.dispatch({type: LANG.SWITCH, payload: tmp});
  storage.set(STORAGE_KEY, tmp);
};

const getT = (domain) => (key, opt) => i18next.t(`${domain}.${key}`, opt);

const isLang = (lang) => i18next.language === lang;

export {getT, setLang, isLang};
