import {getT, setLang} from './lang';
import {switchBrand} from './theme';
import storage from './storage';

const alphabet = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM_-0123456789';
const pick = () => alphabet[Math.floor(Math.random() * alphabet.length)];
const genId = (x = 10) => [...Array(x)].reduce((res) => res + pick(), '');

const re = {
  phone: /(\d)(\d{3})(\d{3})(\d{4})$/,
  anyTag: /<\s*[a-z]+[^>]*>/i,
};

const formatPhone = (raw) => (re.phone.test(raw) ?
  raw.replace(re.phone, '$1-$2-$3-$4') :
  raw
);

const hasHtmlTag = (raw) => re.anyTag.test(raw);

export {genId, getT, setLang, formatPhone, hasHtmlTag, switchBrand, storage};
