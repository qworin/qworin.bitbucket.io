import React from 'react';

import Home from '../content/Home';

const Root = () => <Home />;

export default Root;
