const react = require('eslint-config-airbnb/rules/react');
const reactA11y = require('eslint-config-airbnb/rules/react-a11y');
// const vueRules = require('eslint-plugin-vue/lib');
const rules = {
  'no-console': 'off',
  'no-shadow': 'off',
  'import/prefer-default-export': false,
  'object-curly-newline': ['off', { 'multiline': true }],
  'object-curly-spacing': ['never', { 'objectsInObjects': true }],
  'no-plusplus': ['error', { 'allowForLoopAfterthoughts': true }],
  'arrow-parens': ['error', 'always', {
    requireForBlockBody: true,
  }],
  'global-require': 'off',
  'operator-linebreak': ['error', 'after'],
  'react/jsx-wrap-multilines': ['error', {
    declaration: true,
    assignment: true,
    return: true,
    arrow: true,
  }],
  'react/jsx-curly-brace-presence': ['off', { props: 'always', children: 'always' }],
  'react/forbid-prop-types': 'off',
  'react/jsx-tag-spacing': ['error', {
    closingSlash: 'never',
    beforeSelfClosing: 'always',
    afterOpening: 'never'
  }],
  // 'react/jsx-fragments': ['on', 'syntax'],
  'jsx-a11y/click-events-have-key-events': 'off',
  'jsx-a11y/no-static-element-interactions': 'off',
  'jsx-a11y/anchor-has-content': 'off',
  curly: ['error', 'multi-line'],
}
module.exports = {
  parser: 'babel-eslint',
  extends: ['eslint:recommended', 'airbnb-base', 'plugin:react/recommended'],
  plugins: ['react', 'jsx-a11y', 'import', 'babel'],
  overrides: [
    { 
      files: ['*.js'], 
      rules,
    },
    { 
      files: ['*.jsx'], 
      rules: {...react.rules, ...reactA11y.rules, ...rules},
    },
  ],
  globals: {
    '__DEV__': true,
    window: true,
    console: true,
    module: true,
    GLOBAL: true,
    Promise: true
  },
  env: {
    browser: true,
    node: true
  },
  settings: {
    react: {
      pragma: 'React',
      version: 'detect',
    },
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx']
      }
    }
  },
}