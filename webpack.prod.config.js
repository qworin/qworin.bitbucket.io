const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Promise = require('es6-promise-promise');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

module.exports = {
  mode: 'production',
  devtool: 'nosources-source-map',
  context: path.join(__dirname, './src'),
  entry: {
    app: ['./main.jsx'],
  },
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'bundle.js',
    library: 'index',
    publicPath: '/',
  },
  optimization: {
    minimize: true,
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.json', '.js', '.jsx', '.ts', '.tsx'],
  },
  resolveLoader: {
    modules: ['node_modules'],
    extensions: ['.json', '.js', '.jsx', '.ts', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader?localIdentName=[name][hash:base64:5]',
          'postcss-loader',
        ],
      },
      {
        test: /\.less$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader?localIdentName=[name][hash:base64:5]',
          'postcss-loader',
          'less-loader',
        ],
      },
      {
        test: /\.(eot|woff|woff2|ttf|otf|png|jpe?g|gif)$/,
        loaders: [
          'base64-inline-loader?limit=1000&name=[name].[ext]',
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },

  plugins: [
    new MiniCssExtractPlugin({ filename: 'bundle.css', allChunks: true }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
      Promise: 'es6-promise-promise',
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new HtmlWebpackPlugin({
      title: 'CV',
      template: 'index.html',
      inlineSource: '.(js|css)$',
      favicon: './favicon.ico',
    }),
    new HtmlWebpackInlineSourcePlugin(),

    new OptimizeCssAssetsPlugin({
      cssProccesssorOptions: { discardComments: { removeAll: true } },
    }),
  ],

  devServer: {
    contentBase: __dirname,
    inline: true,
    stats: 'errors-only',
    port: 8081,
    historyApiFallback: true,
    overlay: true,
  },
};
