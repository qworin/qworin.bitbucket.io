const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const path = require('path');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  context: path.join(__dirname, './src'),
  entry: [
    path.join(__dirname, './src/main.jsx'),
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:8888/',
  ],
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'bundle.js',
    library: 'index',
    publicPath: '/',
  },
  watchOptions: { aggregateTimeout: 300, poll: 1000 },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.json', '.js', '.jsx', '.ts', '.tsx'],
  },
  resolveLoader: {
    modules: ['node_modules'],
    extensions: ['.json', '.js', '.jsx', '.ts', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: [
          'css-hot-loader',
          MiniCssExtractPlugin.loader,
          'css-loader?localIdentName=[name][hash:base64:5]',
          'postcss-loader',
        ],
      },
      {
        test: /\.less$/,
        use: [
          'css-hot-loader',
          MiniCssExtractPlugin.loader,
          'css-loader?localIdentName=[name][hash:base64:5]',
          'postcss-loader',
          'less-loader',
        ],
      },
      {
        test: /\.(eot|woff|woff2|ttf|otf)$/,
        loader: 'url-loader?' +
          'limit=30000',
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        loader: 'file-loader?limit=1000',
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin('bundle.css'),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
    new HtmlWebpackPlugin({
      title: 'CV',
      template: 'index.html',
      favicon: './favicon.ico',
    }),
    new OptimizeCssAssetsPlugin({
      cssProccesssorOptions: { discardComments: { removeAll: true } },
    }),
  ],
  devServer: {
    contentBase: __dirname,
    inline: false,
    stats: 'errors-only',
    port: 8081,
    historyApiFallback: true,
    overlay: true,
  },
};
